variable "location" {
  description = "Azure Region where the AKS module should be provisioned"
  default     = "Central US"
}

variable "resource_group_name" {
  description = "Azure Resource Group where the AKS module should be provisioned"
  default     = "hashi-kyndryl-demo-terraform-aks"
}
