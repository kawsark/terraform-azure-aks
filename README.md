## Terraform-azure-aks-winnodepool
The terraform configuration in this repo calls a module called [aks-winnodepool-module](https://gitlab.com/kawsark/terraform-azure-aks-winnodepool-module) to create an AKS cluster with Linux and Windows Node pools.

### How to use
- Publish module: Publish the aks-winnodepool-module referenced above to the Private Module Registry (PMR) of your TFE/TFE Organization. 
  - Please see: [Publishing Modules to the Terraform Cloud Private Module Registry](https://www.terraform.io/docs/cloud/registry/publish.html)
- Fork this repo and update the module source: Update the [main.tf](./main.tf) file with the correct Organization, Module Version as part of the module source. Example shown below.
```bash
# main.tf:
module "aks_winnodepool_module" {
  source  = "app.terraform.io/GitlabCI-demos/aks-winnodepool-module/azure"
  version = "1.1.7"
...
```
- Update module inputs: Update the [main.tf](./main.tf) file with the appropriate inputs. Please consule the [aks-winnodepool-module repo](https://gitlab.com/kawsark/terraform-azure-aks-winnodepool-module) for a full set of inputs.
- Create a Workspace in TFC/TFE and associate it with your fork of this repo. 
  - Please see: [Connecting VCS Providers to Terraform Cloud](https://www.terraform.io/docs/cloud/vcs/index.html).
- Set Terraform Azure credentials as sensitive ARM_* environment variables.
  - Please see: [Specify service principal credentials in environment variables](https://docs.microsoft.com/en-us/azure/developer/terraform/authenticate-to-azure?tabs=bash#specify-service-principal-credentials-in-environment-variables)
  - Also: [Sensitive values](https://www.terraform.io/docs/cloud/workspaces/variables.html#sensitive-values)
- Create a Run in TFE/TFE:
  - Please see: [The UI- and VCS-driven Run Workflow](https://www.terraform.io/docs/cloud/run/ui.html)

### Cleanup
- Queue a Destroy Run in your TFC/TFE Workspace and ensure that all resources are destroyed
- Delete the Workspace from TFC/TFE
  - **Important**: Before Deleting the Workspace, please ensure there are zero resources reflected in the Workspace state file after the destroy Run. 
