module "aks_winnodepool_module" {
  source  = "app.terraform.io/GitlabCI-demos/aks-winnodepool-module/azure"
  version = "1.1.8"

  # Input variables
  environment         = "demo"
  location            = var.location
  resource_group_name = var.resource_group_name
}
